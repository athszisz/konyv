<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Mandelbrot!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>A Mandelbrot halmaz</title>
        <para>
            Megoldás forrása: <link xlink:href="https://progpater.blog.hu/2011/03/26/kepes_egypercesek">https://progpater.blog.hu/2011/03/26/kepes_egypercesek</link>               
        </para>
        <para>
                A Mandelbrot halmazt úgy képezzük, hogy veszünk egy sík origója középpontú
            4 oldalhosszúságú négyzetet, amire egy rácsot fektetünk és kiszámoljuk, hogy a rács pontjai
            mely komplex számoknak felelnek meg. A rács minden pontját megvizsgáljuk.
            A c lesz az éppen vizsgált rácspont, z<subscript>0</subscript> az origó és az alkalmazott képlet:
        </para>
        <itemizedlist>
            <listitem>
                <para>
                    z<subscript>0</subscript> = 0
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>1</subscript> = 0<superscript>2</superscript>+c = c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>2</subscript> = c<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>3</subscript> = (c<superscript>2</superscript>+c)<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>4</subscript> = ((c<superscript>2</superscript>+c)<superscript>2</superscript>+c)<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    ...
                </para>
            </listitem>
        </itemizedlist>
<para>
    Az origóból kiindulva megyünk az első pontba (ez lesz c), majd a c alapján haladunk tovább.
    Ha ez kivezet a kettő sugarú körből, akkor a vizsgált rácspont nem a halmaz eleme.
    Abban az esetben, ha a vizsgálat közben nem megyünk ki a körből, akkor az a Mandelbrot halmaz eleme lesz és feketére színezzük.
</para>
<programlisting language="c"><![CDATA[    
        #include <iostream>
        #include "png++/png.hpp"

        int main (int argc, char *argv[])
        {
            if (argc != 2) {
                std::cout << "Hasznalat: ./mandelpng fajlnev";
                return -1;
            }

            // számítás adatai
            double a = -2.0, b = .7,  c = -1.35, d = 1.35;
            int szelesseg = 600, magassag = 600, iteraciosHatar = 1000;

            // png-t készítünk a png++ csomaggal
            png::image <png::rgb_pixel> kep (szelesseg, magassag);

            // a számítás
            double dx = (b-a)/szelesseg;
            double dy = (d-c)/magassag;
            double reC, imC, reZ, imZ, ujreZ, ujimZ;
            // Hány iterációt csináltunk?
            int iteracio = 0;
            std::cout << "Szamitas";
            // Végigzongorázzuk a szélesség x magasság rácsot:
            for (int j=0; j<magassag; ++j) {
                //sor = j;
                for (int k=0; k<szelesseg; ++k) {
                    // c = (reC, imC) a rács csomópontjainak
                    // megfelelő komplex szám
                    reC = a+k*dx;
                    imC = d-j*dy;
                    // z_0 = 0 = (reZ, imZ)
                    reZ = 0;
                    imZ = 0;
                    iteracio = 0;
                    // z_{n+1} = z_n * z_n + c iterációk
                    // számítása, amíg |z_n| < 2 vagy még
                    // nem értük el a 255 iterációt, ha
                    // viszont elértük, akkor úgy vesszük,
                    // hogy a kiinduláci c komplex számra
                    // az iteráció konvergens, azaz a c a
                    // Mandelbrot halmaz eleme
                    while (reZ*reZ + imZ*imZ < 4 && iteracio < iteraciosHatar) {
                        // z_{n+1} = z_n * z_n + c
                        ujreZ = reZ*reZ - imZ*imZ + reC;
                        ujimZ = 2*reZ*imZ + imC;
                        reZ = ujreZ;
                        imZ = ujimZ;

                        ++iteracio;

                    }

                    kep.set_pixel(k, j, png::rgb_pixel(255-iteracio%256,
                                                       255-iteracio%256, 255-iteracio%256));
                }
                std::cout << "." << std::flush;
            }

            kep.write (argv[1]);
            std::cout << argv[1] << " mentve" << std::endl;
        }
]]></programlisting>
    </section>        
        
    <section>
        <title>A Mandelbrot halmaz a <filename>std::complex</filename> osztállyal</title>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Mandelbrot/3.1.2.cpp">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Mandelbrot/3.1.2.cpp</link>               
        </para>
        <programlisting language="c"><![CDATA[    
        #include <iostream>
        #include "png++/png.hpp"
        #include <complex>
        int
        main ( int argc, char *argv[] )
        {
          int szelesseg = 1920;
          int magassag = 1080;
          int iteraciosHatar = 255;
              
          double a = -1.9;
          double b = 0.7;
          double c = -1.3;
          double d = 1.3;
          if ( argc == 9 )
            {
              szelesseg = atoi ( argv[2] );
              magassag =  atoi ( argv[3] );
              iteraciosHatar =  atoi ( argv[4] );
              a = atof ( argv[5] );
              b = atof ( argv[6] );
              c = atof ( argv[7] );
              d = atof ( argv[8] );
            }
          else
            {
              std::cout << "Hasznalat: ./3.1.2 fajlnev szelesseg magassag n a b c d" << std::endl;
              return -1;
            }
            ]]></programlisting>
            <para>
                    Importáljuk a komplex osztályt, ugyanazokat az adatokat kell bekérni itt is, mint az előző programnál.
            </para>
            <programlisting language="c"><![CDATA[
          png::image < png::rgb_pixel > kep ( szelesseg, magassag );
          double dx = ( b - a ) / szelesseg;
          double dy = ( d - c ) / magassag;
          double reC, imC, reZ, imZ;
          int iteracio = 0;
          std::cout << "Szamitas\n";
          // j megy a sorokon
          for ( int j = 0; j < magassag; ++j )
            {
              // k megy az oszlopokon
              for ( int k = 0; k < szelesseg; ++k )
                {
                  // c = (reC, imC) a halo racspontjainak
                  // megfelelo komplex szam
                  reC = a + k * dx;
                  imC = d - j * dy;
                  std::complex<double> c ( reC, imC );
                  std::complex<double> z_n ( 0, 0 );
                ]]></programlisting>
            <para>
                    A reC és imC segítségével a háló rácspontjait és az ezekhez tartozó számot számoljuk ki. Ezek komplex számok, itt használjuk az include-olt complex osztályt.
            </para>
            <programlisting language="c"><![CDATA[
                  iteracio = 0;
                  while ( std::abs ( z_n ) < 4 && iteracio < iteraciosHatar )
                    {
                      z_n = z_n * z_n + c;
                      ++iteracio;
                    }
                ]]></programlisting>
            <para>
                    A while legfeljebb az iteraciosHatar-szor fut le. Azt nézzük, hogy a z_n mennyire távolodik el a z_0-tól. Az adott pontot feketére színezzük akkor, ha a while ciklusból az iteráéciós határ elérése miatt lépünk ki. 
                Ekkor az adott pontban az iteráció konvergens így a mandelbrot halmaz eleme.
            </para>
            <programlisting language="c"><![CDATA[
                  kep.set_pixel ( k, j,
                                  png::rgb_pixel ( iteracio%255, (iteracio*iteracio)%255, 0 ) );
                }
              int szazalek = ( double ) j / ( double ) magassag * 100.0;
              std::cout << "\r" << szazalek << "%" << std::flush;
            }
          kep.write ( argv[1] );
          std::cout << "\r" << argv[1] << " mentve." << std::endl;
        }
]]></programlisting>
<para>
    Kimentjük a kapott halmazt.
</para>
<mediaobject>
    <imageobject>
        <imagedata fileref="/home/zsani/progz/Mandelbrot/mandelke.png" scale="20"/>
            </imageobject>
     </mediaobject>
    </section>        
                
    <section>
        <title>Biomorfok</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/IJMbgRzY76E">https://youtu.be/IJMbgRzY76E</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/Biomorf">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/Biomorf</link>
        </para>
        <programlisting language="c++"><![CDATA[    
        #include <iostream>
        #include "png++/png.hpp"
        #include <complex>
        int
        main ( int argc, char *argv[] )
        {
            int szelesseg = 1920;
            int magassag = 1080;
            int iteraciosHatar = 255;
            double xmin = -1.9;
            double xmax = 0.7;
            double ymin = -1.3;
            double ymax = 1.3;
            double reC = .285, imC = 0;
            double R = 10.0;
            if ( argc == 12 )
            {
                szelesseg = atoi ( argv[2] );
                magassag =  atoi ( argv[3] );
                iteraciosHatar =  atoi ( argv[4] );
                xmin = atof ( argv[5] );
                xmax = atof ( argv[6] );
                ymin = atof ( argv[7] );
                ymax = atof ( argv[8] );
                reC = atof ( argv[9] );
                imC = atof ( argv[10] );
                R = atof ( argv[11] );
            }
            else
            {
                std::cout << "Hasznalat: ./3.1.2 fajlnev szelesseg magassag n a b c d reC imC R" << std::endl;
                return -1;
            }
            png::image < png::rgb_pixel > kep ( szelesseg, magassag );
            double dx = ( xmax - xmin ) / szelesseg;
            double dy = ( ymax - ymin ) / magassag;
            std::complex<double> c ( reC, imC );
            std::cout << "Szamitas\n";
            // j megy a sorokon
            for ( int y = 0; y < magassag; ++y )
            {
                // k megy az oszlopokon
                for ( int x = 0; x < szelesseg; ++x )
                {
                    double reZ = xmin + x * dx;
                    double imZ = ymax - y * dy;
                    std::complex<double> z_n ( reZ, imZ );
                    int iteracio = 0;
                    for (int i=0; i < iteraciosHatar; ++i)
                    {
                        z_n = std::pow(z_n, 3) + c;
                        //z_n = std::pow(z_n, 2) + std::sin(z_n) + c;
                        if(std::real ( z_n ) > R || std::imag ( z_n ) > R)
                        {
                            iteracio = i;
                            break;
                        }
                    }
                    kep.set_pixel ( x, y,
                                    png::rgb_pixel ( (iteracio*20)%255, (iteracio*40)%255, (iteracio*60)%255 ));
                }
                int szazalek = ( double ) y / ( double ) magassag * 100.0;
                std::cout << "\r" << szazalek << "%" << std::flush;
            }
            kep.write ( argv[1] );
            std::cout << "\r" << argv[1] << " mentve." << std::endl;
        }
]]></programlisting> 

<para>
    A Mandelbrot halmaztól eltérően a Julia halmazoknál a c nem változik hanem minden vizsgált z rácspontra ugyanaz.
</para>
        <mediaobject>
    <imageobject>
        <imagedata fileref="/home/zsani/progz/Mandelbrot/bmorf.png" scale="40"/>
            </imageobject>
     </mediaobject>
                               
    </section>                     

    <section>
        <title>A Mandelbrot halmaz CUDA megvalósítása</title>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/CUDA">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/CUDA</link>
        </para>
        <para>
            A CPU helyett az nvidia kártyánk CUDA magjait használjuk. Ahhoz, hogy egyszerre számoljuk 60 darab blokkot kell beraknunk minden sorba és oszlopba majd blokkonként száz szálat. 600x600 pixeles képet 
            szeretnénk számolni úgy, hogy minden pixel külön szálon számolódjon. Ez jelentős előny a CPU-val szemben ahol egy szálon számoltunk.
        </para>
           <programlisting><![CDATA[
            #include <png++/image.hpp>
            #include <png++/rgb_pixel.hpp>

            #include <sys/times.h>
            #include <iostream>


            #define MERET 600
            #define ITER_HAT 32000

            __device__ int
            mandel (int k, int j)
            {
              // Végigzongorázza a CUDA a szélesség x magasság rácsot:
              // most eppen a j. sor k. oszlopaban vagyunk

              // számítás adatai
              float a = -2.0, b = .7, c = -1.35, d = 1.35;
              int szelesseg = MERET, magassag = MERET, iteraciosHatar = ITER_HAT;

              // a számítás
              float dx = (b - a) / szelesseg;
              float dy = (d - c) / magassag;
              float reC, imC, reZ, imZ, ujreZ, ujimZ;
              // Hány iterációt csináltunk?
              int iteracio = 0;

              // c = (reC, imC) a rács csomópontjainak
              // megfelelő komplex szám
              reC = a + k * dx;
              imC = d - j * dy;
              // z_0 = 0 = (reZ, imZ)
              reZ = 0.0;
              imZ = 0.0;
              iteracio = 0;
              // z_{n+1} = z_n * z_n + c iterációk
              // számítása, amíg |z_n| < 2 vagy még
              // nem értük el a 255 iterációt, ha
              // viszont elértük, akkor úgy vesszük,
              // hogy a kiinduláci c komplex számra
              // az iteráció konvergens, azaz a c a
              // Mandelbrot halmaz eleme
              while (reZ * reZ + imZ * imZ < 4 && iteracio < iteraciosHatar)
                {
                  // z_{n+1} = z_n * z_n + c
                  ujreZ = reZ * reZ - imZ * imZ + reC;
                  ujimZ = 2 * reZ * imZ + imC;
                  reZ = ujreZ;
                  imZ = ujimZ;

                  ++iteracio;

                }
              return iteracio;
            }


            /*
            __global__ void
            mandelkernel (int *kepadat)
            {

              int j = blockIdx.x;
              int k = blockIdx.y;

              kepadat[j + k * MERET] = mandel (j, k);

            }
            */

            __global__ void
            mandelkernel (int *kepadat)
            {

              int tj = threadIdx.x;
              int tk = threadIdx.y;

              int j = blockIdx.x * 10 + tj;
              int k = blockIdx.y * 10 + tk;

              kepadat[j + k * MERET] = mandel (j, k);

            }

            void
            cudamandel (int kepadat[MERET][MERET])
            {

              int *device_kepadat;
              cudaMalloc ((void **) &device_kepadat, MERET * MERET * sizeof (int));

              // dim3 grid (MERET, MERET);
              // mandelkernel <<< grid, 1 >>> (device_kepadat);

              dim3 grid (MERET / 10, MERET / 10);
              dim3 tgrid (10, 10);
              mandelkernel <<< grid, tgrid >>> (device_kepadat);  

              cudaMemcpy (kepadat, device_kepadat,
                          MERET * MERET * sizeof (int), cudaMemcpyDeviceToHost);
              cudaFree (device_kepadat);

            }

            int
            main (int argc, char *argv[])
            {

              // Mérünk időt (PP 64)
              clock_t delta = clock ();
              // Mérünk időt (PP 66)
              struct tms tmsbuf1, tmsbuf2;
              times (&tmsbuf1);

              if (argc != 2)
                {
                  std::cout << "Hasznalat: ./mandelpngc fajlnev";
                  return -1;
                }

              int kepadat[MERET][MERET];

              cudamandel (kepadat);

              png::image < png::rgb_pixel > kep (MERET, MERET);

              for (int j = 0; j < MERET; ++j)
                {
                  //sor = j;
                  for (int k = 0; k < MERET; ++k)
                    {
                      kep.set_pixel (k, j,
                                     png::rgb_pixel (255 -
                                                     (255 * kepadat[j][k]) / ITER_HAT,
                                                     255 -
                                                     (255 * kepadat[j][k]) / ITER_HAT,
                                                     255 -
                                                     (255 * kepadat[j][k]) / ITER_HAT));
                    }
                }
              kep.write (argv[1]);

              std::cout << argv[1] << " mentve" << std::endl;

              times (&tmsbuf2);
              std::cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
                + tmsbuf2.tms_stime - tmsbuf1.tms_stime << std::endl;

              delta = clock () - delta;
              std::cout << (float) delta / CLOCKS_PER_SEC << " sec" << std::endl;

            }
]]></programlisting> 
    </section>                     

    <section>
        <title>Mandelbrot nagyító és utazó C++ nyelven</title>
        <para>
            Építs GUI-t a Mandelbrot algoritmusra, lehessen egérrel nagyítani egy területet, illetve egy pontot
            egérrel kiválasztva vizualizálja onnan a komplex iteréció bejárta z<subscript>n</subscript> komplex számokat!
        </para>
        <para>Labor passz</para>
                <mediaobject>
    <imageobject>
        <imagedata fileref="/home/zsani/progz/passz.jpg" scale="25"/>
            </imageobject>
     </mediaobject>
            
        <para>
            Megoldás forrása:  
        </para>

        <para>
            Megoldás videó: 
        </para>
        <para>
            Megoldás forrása:  
        </para>
    </section>                     
                                                                                                                                                                            
    <section>
        <title>Mandelbrot nagyító és utazó Java nyelven</title>
        <para>
            A program futtatása után a jobb egérgomb segítségével Mandelbrot halmazon belül 
            vizualizálja a komplexszámokat.
            Ezek azok a számok amelyek nem mennek ki a végtelenbe.          
        </para>
        <mediaobject>
    <imageobject>
        <imagedata fileref="/home/zsani/progz/Mandelbrot/mandelnagyitjobb.png" scale="60"/>
            </imageobject>
     </mediaobject>
     <para>
         A bal egérgombbal pedig kinagyíthatjuk a kijelölt részünket.
         Abban az esetben, ha a pontosság leromlott akkor az N betű segítségével növelni tudjuk a számítások iterációs határát.
     </para>
     <mediaobject>
    <imageobject>
        <imagedata fileref="/home/zsani/progz/Mandelbrot/mandelnagyitbal.png" scale="28"/>
            </imageobject>
     </mediaobject>
     <para>
         MandelbrotHalmazNagyító.java:
     </para>
     <programlisting><![CDATA[
           public class MandelbrotHalmazNagyító extends MandelbrotHalmaz {
               /** A nagyítandó kijelölt területet bal felsõ sarka. */
               private int x, y;
               /** A nagyítandó kijelölt terület szélessége és magassága. */
               private int mx, my;
               /**
                * Létrehoz egy a Mandelbrot halmazt a komplex sík
                * [a,b]x[c,d] tartománya felett kiszámoló és nygítani tudó
                * <code>MandelbrotHalmazNagyító</code> objektumot.
                *
                * @param      a              a [a,b]x[c,d] tartomány a koordinátája.
                * @param      b              a [a,b]x[c,d] tartomány b koordinátája.
                * @param      c              a [a,b]x[c,d] tartomány c koordinátája.
                * @param      d              a [a,b]x[c,d] tartomány d koordinátája.
                * @param      szélesség      a halmazt tartalmazó tömb szélessége.
                * @param      iterációsHatár a számítás pontossága.
                */
               public MandelbrotHalmazNagyító(double a, double b, double c, double d,
                       int szélesség, int iterációsHatár) {
                   // Az õs osztály konstruktorának hívása
                   super(a, b, c, d, szélesség, iterációsHatár);
                   setTitle("A Mandelbrot halmaz nagyításai");
                   // Egér kattintó események feldolgozása:
                   addMouseListener(new java.awt.event.MouseAdapter() {
                       // Egér kattintással jelöljük ki a nagyítandó területet
                       // bal felsõ sarkát vagy ugyancsak egér kattintással
                       // vizsgáljuk egy adott pont iterációit:
                       public void mousePressed(java.awt.event.MouseEvent m) {
                           // Az egérmutató pozíciója
                           x = m.getX();
                           y = m.getY();
                           // Az 1. egér gombbal a nagyítandó terület kijelölését
                           // végezzük:
                           if(m.getButton() == java.awt.event.MouseEvent.BUTTON1 ) {
                               // A nagyítandó kijelölt területet bal felsõ sarka: (x,y)
                               // és szélessége (majd a vonszolás növeli)
                               mx = 0;
                               my = 0;
                               repaint();
                           } else {
                               // Nem az 1. egér gombbal az egérmutató mutatta c
                               // komplex számból indított iterációkat vizsgálhatjuk
                               MandelbrotIterációk iterációk =
                                       new MandelbrotIterációk(
                                       MandelbrotHalmazNagyító.this, 50);
                               new Thread(iterációk).start();
                           }
                       }
                       // Vonszolva kijelölünk egy területet...
                       // Ha felengedjük, akkor a kijelölt terület
                       // újraszámítása indul:
                       public void mouseReleased(java.awt.event.MouseEvent m) {
                           if(m.getButton() == java.awt.event.MouseEvent.BUTTON1 ) {
                               double dx = (MandelbrotHalmazNagyító.this.b
                                       - MandelbrotHalmazNagyító.this.a)
                                       /MandelbrotHalmazNagyító.this.szélesség;
                               double dy = (MandelbrotHalmazNagyító.this.d
                                       - MandelbrotHalmazNagyító.this.c)
                                       /MandelbrotHalmazNagyító.this.magasság;
                               // Az új Mandelbrot nagyító objektum elkészítése:
                               new MandelbrotHalmazNagyító(
                                       MandelbrotHalmazNagyító.this.a+x*dx,
                                       MandelbrotHalmazNagyító.this.a+x*dx+mx*dx,
                                       MandelbrotHalmazNagyító.this.d-y*dy-my*dy,
                                       MandelbrotHalmazNagyító.this.d-y*dy,
                                       600,
                                       MandelbrotHalmazNagyító.this.iterációsHatár);
                           }
                       }
                   });
                   // Egér mozgás események feldolgozása:
                   addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
                       // Vonszolással jelöljük ki a négyzetet:
                       public void mouseDragged(java.awt.event.MouseEvent m) {
                           // A nagyítandó kijelölt terület szélessége és magassága:
                           mx = m.getX() - x;
                           my = m.getY() - y;
                           repaint();
                       }
                   });
               }
               /**
                * Pillanatfelvételek készítése.
                */
               public void pillanatfelvétel() {
                   // Az elmentendõ kép elkészítése:
                   java.awt.image.BufferedImage mentKép =
                           new java.awt.image.BufferedImage(szélesség, magasság,
                           java.awt.image.BufferedImage.TYPE_INT_RGB);
                   java.awt.Graphics g = mentKép.getGraphics();
                   g.drawImage(kép, 0, 0, this);
                   g.setColor(java.awt.Color.BLACK);
                   g.drawString("a=" + a, 10, 15);
                   g.drawString("b=" + b, 10, 30);
                   g.drawString("c=" + c, 10, 45);
                   g.drawString("d=" + d, 10, 60);
                   g.drawString("n=" + iterációsHatár, 10, 75);
                   if(számításFut) {
                       g.setColor(java.awt.Color.RED);
                       g.drawLine(0, sor, getWidth(), sor);
                   }
                   g.setColor(java.awt.Color.GREEN);
                   g.drawRect(x, y, mx, my);
                   g.dispose();
                   // A pillanatfelvétel képfájl nevének képzése:
                   StringBuffer sb = new StringBuffer();
                   sb = sb.delete(0, sb.length());
                   sb.append("MandelbrotHalmazNagyitas_");
                   sb.append(++pillanatfelvételSzámláló);
                   sb.append("_");
                   // A fájl nevébe belevesszük, hogy melyik tartományban
                   // találtuk a halmazt:
                   sb.append(a);
                   sb.append("_");
                   sb.append(b);
                   sb.append("_");
                   sb.append(c);
                   sb.append("_");
                   sb.append(d);
                   sb.append(".png");
                   // png formátumú képet mentünk
                   try {
                       javax.imageio.ImageIO.write(mentKép, "png",
                               new java.io.File(sb.toString()));
                   } catch(java.io.IOException e) {
                       e.printStackTrace();
                   }
               }
               /**
                * A nagyítandó kijelölt területet jelzõ négyzet kirajzolása.
                */
               public void paint(java.awt.Graphics g) {
                   // A Mandelbrot halmaz kirajzolása
                   g.drawImage(kép, 0, 0, this);
                   // Ha éppen fut a számítás, akkor egy vörös
                   // vonallal jelöljük, hogy melyik sorban tart:
                   if(számításFut) {
                       g.setColor(java.awt.Color.RED);
                       g.drawLine(0, sor, getWidth(), sor);
                   }
                   // A jelzõ négyzet kirajzolása:
                   g.setColor(java.awt.Color.GREEN);
                   g.drawRect(x, y, mx, my);
               }
               /**
                * Hol áll az egérmutató?
                * @return int a kijelölt pont oszlop pozíciója.
                */    
               public int getX() {
                   return x;
               }
               /**
                * Hol áll az egérmutató?
                * @return int a kijelölt pont sor pozíciója.
                */    
               public int getY() {
                   return y;
               }
               /**
                * Példányosít egy Mandelbrot halmazt nagyító obektumot.
                */
               public static void main(String[] args) {
                   // A kiinduló halmazt a komplex sík [-2.0, .7]x[-1.35, 1.35]
                   // tartományában keressük egy 600x600-as hálóval és az
                   // aktuális nagyítási pontossággal:
                   new MandelbrotHalmazNagyító(-2.0, .7, -1.35, 1.35, 600, 255);
               }
           } 
     ]]></programlisting> 
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
